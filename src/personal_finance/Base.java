/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personal_finance;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Field;
import javax.swing.*;

/**
 *
 * @author Lenovo
 */
public class Base extends JFrame implements ActionListener, MouseListener {

    public Base() {
        super("Avocado v0.1- Daily Financial");
    }

    public void actionPerformed(ActionEvent event) {
    }

    public boolean is_required(String value) {
        return (value.equalsIgnoreCase("")) ? true : false;
    }

    public String is_required_message(String key) {
        return key + " is required.<br/>";
    }

    public boolean is_int(String value) {
        boolean result = false;
        try {
            int var_int = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            result = true;
        }
        return result;
    }

    public String is_int_message(String key) {
        return key + " is not number.<br/>";
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel(
                    "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"
            );
        } catch (Exception exc) {
            // ignore error
        }
    }

}
