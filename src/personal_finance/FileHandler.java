/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personal_finance;

import java.io.*;

/**
 *
 * @author Lenovo
 */
public class FileHandler {

    String file_path = "";

    public FileHandler(String path) {
        file_path = path;
    }

    void write(String output) throws IOException {
        try {
            output = output;
            byte[] data = output.getBytes();
            File file = new File(file_path);
            FileOutputStream fileStream = new FileOutputStream(file);
            fileStream.write(data, 0, data.length);
            fileStream.close();
        } catch (IOException e) {
            System.out.println("Could not write file");
        }
    }

}
