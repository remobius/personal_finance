/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personal_finance;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.swing.*;
import java.sql.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.text.html.parser.DTDConstants;

/**
 *
 * @author Lenovo
 */
public class Transaction extends Base {

    JPanel panel_main = new JPanel();
    JPanel panel_table = new JPanel();
    JPanel panel_pagination = new JPanel();
    JPanel panel_validation = new JPanel();
    JPanel panel_form_create = new JPanel();
    JPanel panel_form_edit = new JPanel();
    JPanel panel_toolbar = new JPanel();
    JTable table = new JTable();
    Map<String, String> employee_list = new HashMap<String, String>();
    Map<String, String> status_list = new HashMap<String, String>();
    // Panel - Buttons
    JButton btn_create = new JButton("Create");
    JButton btn_edit = new JButton("Edit");
    JButton btn_delete = new JButton("Delete");
    // Panel - Toolbar
    JButton btn_save_file = new JButton("Save");
    JButton btn_open_file = new JButton("Open");
    JButton btn_exit = new JButton("Exit");
    // Panel - Pagination
    JButton btn_first = new JButton("First");
    JButton btn_prev = new JButton("Prev");
    JButton btn_next = new JButton("Next");
    JButton btn_last = new JButton("Last");
    JLabel label_pagination = new JLabel("", JLabel.LEFT);
    // Validation
    JLabel label_validation_error = new JLabel("", JLabel.LEFT);
    // Form - Filter
    JLabel label_search = new JLabel("Search:", JLabel.LEFT);
    JTextField input_search = new JTextField();
    // Form - Create
    JLabel label_description = new JLabel("Description", JLabel.LEFT);
    JTextField input_description = new JTextField();
    JLabel label_date = new JLabel("Date", JLabel.LEFT);
    JTextField input_date = new JTextField();
    JLabel label_amount = new JLabel("Amount", JLabel.LEFT);
    JTextField input_amount = new JTextField();
    JButton btn_save = new JButton("Save");
    // Form - Edit
    JTextField input_id_update = new JTextField();
    JLabel label_id_employee_update = new JLabel("Name", JLabel.LEFT);
    JComboBox input_id_employee_update = new JComboBox();
    JLabel label_subject_update = new JLabel("Subject", JLabel.LEFT);
    JTextField input_subject_update = new JTextField();
    JLabel label_description_update = new JLabel("Description", JLabel.LEFT);
    JTextArea input_description_update = new JTextArea();
    JLabel label_id_status_update = new JLabel("Status", JLabel.LEFT);
    JComboBox input_id_status_update = new JComboBox();
    JButton btn_update = new JButton("Update");
    String address = "jdbc:mysql://localhost:3306/helpdesk";
    String username = "root";
    String password = "root";
    Map<String, String> validation_errors = new HashMap<String, String>();
    String validation_error_message = "";

    // Pagination
    int page = 1;
    int number_of_record = 10;
    int offset = 0;
    int last_page = 1;
    String condition = "";
    String LS = System.getProperty("line.separator");
    String file_path = "";
    String cache = "";
    String error_info = "There's something error occurs. Please try again.";
    String current_file = "";

    JPopupMenu menu_table = new JPopupMenu("Table");

    public void setup_table() {
        DefaultTableModel model = new DefaultTableModel();
        // Add columns
        model.addColumn("Date");
        model.addColumn("Description");
        model.addColumn("Amount");
        try {
            if (!cache.equalsIgnoreCase("")) {
                String[] records = cache.split("\r\n");
                for (int i = records.length; i >= 1; i--) {
                    // Add row
                    int key = i - 1;
                    String[] values = records[key].split(",");
                    model.addRow(new Object[]{values[1], values[0], values[2]});
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, error_info);
        }
        table.setModel(model);
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(100);
        columnModel.getColumn(1).setPreferredWidth(1000);
        columnModel.getColumn(2).setPreferredWidth(300);
    }

    public Transaction() {
        setLookAndFeel();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setup_table();
        // Context Menu
        JMenuItem menu_table_delete = new JMenuItem("Delete");
        menu_table_delete.setName("menu_table_delete");
        menu_table_delete.addActionListener(this);
        menu_table.add(menu_table_delete);
        // Main Layout
        BoxLayout layout = new BoxLayout(panel_main, BoxLayout.Y_AXIS);
        panel_main.setLayout(layout);
        // Layout - Table
        GridLayout layout_table = new GridLayout(1, 1, 10, 10);
        panel_table.setLayout(layout_table);
        panel_table.add(new JScrollPane(table));
        // Layout - Validation
        GridLayout layout_validation = new GridLayout(1, 1, 10, 10);
        panel_validation.setLayout(layout_validation);
        panel_validation.add(label_validation_error);
        // Layout - Form Create
        label_description.setPreferredSize(new Dimension(400, 20));
        input_description.setPreferredSize(new Dimension(400, 20));
        label_date.setPreferredSize(new Dimension(400, 20));
        input_date.setPreferredSize(new Dimension(400, 20));
        label_amount.setPreferredSize(new Dimension(400, 20));
        input_amount.setPreferredSize(new Dimension(400, 20));
        btn_save.setName("btn_save");
        GridLayout layout_form_create = new GridLayout(7, 1, 10, 10);
        panel_form_create.setLayout(layout_form_create);
        panel_form_create.add(label_description);
        panel_form_create.add(input_description);
        panel_form_create.add(label_date);
        panel_form_create.add(input_date);
        panel_form_create.add(label_amount);
        panel_form_create.add(input_amount);
        panel_form_create.add(btn_save);
        // Layout - Toolbar
        FlowLayout layout_toolbar = new FlowLayout(FlowLayout.RIGHT, 10, 10);
        panel_toolbar.setLayout(layout_toolbar);
        panel_toolbar.add(btn_save_file);
        panel_toolbar.add(btn_open_file);
        panel_toolbar.add(btn_exit);
        // Add panel to main panel
        panel_main.add(panel_table);
        panel_main.add(panel_validation);
        panel_main.add(panel_form_create);
        panel_main.add(panel_toolbar);
        add(panel_main);
        label_validation_error.setForeground(Color.red);
        btn_save_file.setName("btn_save_file");
        btn_open_file.setName("btn_open_file");
        btn_exit.setName("btn_exit");
        // Add Event Listener
        btn_save.addActionListener(this);
        btn_save_file.addActionListener(this);
        btn_open_file.addActionListener(this);
        btn_exit.addActionListener(this);
        table.addMouseListener(this);
        // Set window or jframe scrollable
        Container container = getContentPane();
        JScrollPane scroll = new JScrollPane(container);
        setContentPane(scroll);
        setVisible(true);
    }

    public void reset() {
        label_validation_error.setText("");
        input_description.setText("");
        input_date.setText("");
        input_amount.setText("");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Transaction transaction_page = new Transaction();
    }

    public void actionPerformed(ActionEvent event) {
        super.actionPerformed(event);
        JComponent source = (JComponent) event.getSource();
        String component_name = source.getName();
        switch (component_name) {
            case "menu_table_delete":
                delete_row();
                break;
            case "btn_save":
                insert();
                break;
            case "btn_save_file":
                save_file();
                break;
            case "btn_open_file":
                load_file();
                break;
            case "btn_exit":
                System.exit(0);
                break;
        }
    }

    public void create() {
        panel_form_edit.setVisible(false);
        panel_form_create.setVisible(true);
    }

    private void load_file() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Comma-separated values file (*.csv)", "csv");
        chooser.setFileFilter(filter);
        int result = chooser.showOpenDialog(this);
        if (result == JFileChooser.CANCEL_OPTION) {
            return;
        }
        try {
            File file = chooser.getSelectedFile();
            file_path = file.toString();
            FileInputStream fin = new FileInputStream(file_path);
            current_file = file_path;
            int i = 0;
            cache = "";
            while ((i = fin.read()) != -1) {
                cache += (char) i;
            }
            fin.close();
            setup_table();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, error_info);
        }
    }

    public void save_file() {
        try {
            if (current_file.equalsIgnoreCase("")) {
                JFileChooser chooser = new JFileChooser();
                chooser.setSelectedFile(new File("Untitled.csv"));
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Comma-separated values file (*.csv)", "csv");
                chooser.setFileFilter(filter);
                int result = chooser.showSaveDialog(this);
                if (result == JFileChooser.CANCEL_OPTION) {
                    return;
                }
                File file = chooser.getSelectedFile();
                file_path = file.toString();
                current_file = file_path;
            }
            FileHandler filehandler = new FileHandler(current_file);
            filehandler.write(cache);
            JOptionPane.showMessageDialog(null, "Data has been saved into a file");
            setup_table();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, error_info);
        }
    }

    public boolean validate_insert() {
        int validation_errors = 0;
        validation_error_message = "";
        String description_text = this.input_description.getText();
        if (is_required(description_text)) {
            validation_error_message += is_required_message("Description");
            validation_errors++;
        }
        String date_text = this.input_date.getText();
        if (is_required(date_text)) {
            validation_error_message += is_required_message("Date");
            validation_errors++;
        }
        String amount_text = this.input_amount.getText();
        if (is_required(amount_text)) {
            validation_error_message += is_required_message("Amount");
            validation_errors++;
        }
        if (is_int(amount_text)) {
            validation_error_message += is_int_message("Amount");
            validation_errors++;
        }
        if (validation_errors == 0) {
            label_validation_error.setText("");
            return true;
        } else {
            label_validation_error.setText("<html>" + validation_error_message + "</html>");
            return false;
        }
    }

    public void insert() {
        if (validate_insert()) {
            try {
                String record = input_description.getText() + "," + input_date.getText()
                        + "," + input_amount.getText() + LS;
                cache += record;
                JOptionPane.showMessageDialog(null, "Data has been saved successfully");
                reset();
                setup_table();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, error_info);
            }
        }
    }

    private void delete() {
        int row = table.getSelectedRow();
        String id = table.getValueAt(row, 0).toString();
        try {
            String sql = "DELETE FROM tickets WHERE id='" + id + "'";
            Connection con = DriverManager.getConnection(address, username, password);
            PreparedStatement pst = con.prepareStatement(sql);
            pst.execute();
            JOptionPane.showMessageDialog(null, "Data berhasil dihapus.");
            setup_table();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void showContextMenuTable(MouseEvent e) {
        menu_table.show(e.getComponent(), e.getX(), e.getY());
    }

    public void delete_row() {
        int row = table.getSelectedRow();
        String[] records = cache.split("\r\n");
        cache = "";
        int total_element = records.length;
        int row_index = total_element - row;
        row_index--;
        for (int i = 0; i < records.length; i++) {
            if (i == row_index) {
                continue;
            }
            cache += records[i] + LS;
        }
        System.out.println(cache);
        setup_table();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int row = table.getSelectedRow();
        if (e.getButton() == MouseEvent.BUTTON3 && row != -1) {
            showContextMenuTable(e);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    private FileInputStream FileInputStream(File file) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void Base(String personal_Finance) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
